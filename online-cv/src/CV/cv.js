export const CV = {
  person: {
    name: "Filipe José",
    adress: "Ferreira Lopes",
    city: "Guimarães / Vila Nova de Sande",
    email: "filipe5lopes@hotmail.com",
    birthDate: "17/04/1999",
    phone: "(+351) 918276505",
    image:
      "https://scontent.fopo3-1.fna.fbcdn.net/v/t1.6435-9/81765681_2900460310018795_7935363928068980736_n.jpg?_nc_cat=106&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=n4AhwBApCTAAX-5BgJq&_nc_ht=scontent.fopo3-1.fna&oh=b5795e5ecf5a42e73a312caed25e28b3&oe=619E306E",
    gitLab: "https://gitlab.com/SrCopes",
    aboutMe: [
      {
        info: "A 22 years old person that knows how to make games! ",
      },
      {
        info: "Always a student.",
      },
      {
        info: "Good in many sports, but futsal as my better quality in terms of sport!",
      },
    ],
  },
  education: [
    {
      name: "Degree on engeniring in digital game development",
      date: "2018 - 2021",
      where: "IPCA",
    },
    {
      name: "Build Multiplayer Games With Unity And Photon (Pun2) by Tevfik Ufuk DEMİRBAŞ, IRONHEAD Games",
      date: "2021",
      where: "Udemy",
    },
    {
      name: "Intro to Multiplayer Game Development with Unity",
      date: "2021",
      where: "Zenva",
    },
  ],
  experience: [
    {
      name: "Intern at Nerd Monkeys",
      date: "07/2021 – 08/2021",
      where: "🗺️ Nerd Monkeys",
      description:
        "My internship consisted on working (under the orientation of the team leader) with unreal engine using blueprints on the team project. SCRUM work base methodology. Constant meetings with an active voice in decisons. First experience with Unreal Engine and blueprints. First experience in a videogame company.",
    },
    {
      name: "Coffee shop worker",
      date: "02/2020 – 05/2021",
      where: "🗺️ Curva Café",
      description:
        "Know how to serve a customer, work in the kitchen, work with pressure and with timings",
    },
  ],
  languages: [
    {
      language: "🇵🇹 Portuguese",
      wrlevel: "Native",
      splevel: "Native",
    },
    {
      language: "🇬🇧 English",
      wrlevel: "B1",
      splevel: "B1",
    },
  ],
  habilities: [
    "Make games",
    "3D modeling",
    "Good at sports",
    "Good sence of justice",
    "Creating new worlds",
  ],
  outsideActivities: [
    {
      name: "⚜️ Scout",
      where: "Guimarães (Agr: 662)",
      description:
        "Like the sentence : the duty of the scout starts at home, we work to serve the community and help everyone!",
    },
    {
      name: "⚽ Futsal player",
      where: "Guimaraes / Vila Nova de Sande",
      description:
        "Played 11 years until 18 years old, be captain many times. Stoped 4 years and returned now at my 22 years of age in a senior level!",
    },
  ],
};
