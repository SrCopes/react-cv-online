import React, { useState } from "react";
import "./App.css";
import About from "./components/about";
import Education from "./components/education";
import Experience from "./components/experience";
import Person from "./components/me";
import More from "./components/more";
import { CV } from "./CV/cv";

const {
  person,
  education,
  experience,
  languages,
  habilities,
  outsideActivities,
} = CV;

export default function App() {
  const [showEducation, setShowEducation] = useState(true);

  return (
    <div className="App">
      <Person person={person} />
      <About aboutMe={person} />
      <button
        className="custom-btn btn-4"
        onClick={() => setShowEducation(true)}
      >
        Education
      </button>
      <button
        className="custom-btn btn-4"
        onClick={() => setShowEducation(false)}
      >
        Experience
      </button>
      <div>
        {showEducation ? (
          <Education education={education} />
        ) : (
          <Experience experience={experience} />
        )}
      </div>
      <More
        languages={languages}
        habilities={habilities}
        outsideActivities={outsideActivities}
      />
    </div>
  );
}
