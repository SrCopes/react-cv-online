import React from "react";
import "./css/Experience.css";

const Experience = ({ experience }) => {
  return (
    <div>
      <h3 style={{ color: "white" }}>Experience</h3>
      <div className="experience-card">
        {experience.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <h4>👨‍💻 {item.name}</h4>
              <p>
                <strong>⌚ duration:</strong> {item.date}
              </p>
              <p>
                <strong>🏢 company:</strong> {item.where}
              </p>
              <strong>Description:</strong>
              <p>{item.description}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Experience;
