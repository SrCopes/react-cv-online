import React from "react";
import "./person.css";

const Person = ({ person }) => {
  return (
    <div className="person">
      <img src={person.image} alt="" />
      <div className="card">
        <h2>
          {person.name} {person.adress}
        </h2>
        <p>🗺️{person.city} </p>
        <p>🗓️{person.birthDate}</p>
        <p>
          📧
          <a href={"email:" + person.email}>filipe5lopes@hotmail.com</a>
        </p>
        <p>📱 {person.phone}</p>
        <p>
          💾<a href={person.gitLab}>GitHub</a>
        </p>
      </div>
    </div>
  );
};

export default Person;
