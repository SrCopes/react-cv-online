import React from "react";
import "./css/AboutMe.css";

const About = ({ aboutMe }) => {
  return (
    <>
      <h4 style={{ color: "white" }}>about me:</h4>
      <div className="about-card">
        {aboutMe.aboutMe.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <p>{item.info}</p>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default About;
