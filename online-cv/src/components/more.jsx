import React, { useState } from "react";
import "./education.css";
import "./css/LanguageCard.css";
import "./css/OutdoorActivities.css";
import "./css/Skills.css";

export default function More({ languages, outsideActivities, habilities }) {
  return (
    <div>
      <h3 style={{ color: "white" }}>Outdoor activities</h3>
      <div className="outdoor-card">
        {outsideActivities.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <h4> {item.name}</h4>
              <p>
                <strong>🗺️ where: </strong> {item.where}
              </p>
              <strong>Description of the activitie:</strong>
              <p>{item.description}</p>
            </div>
          );
        })}
      </div>
      <h3 style={{ color: "white" }}>Languages</h3>
      <div className="language-card">
        {languages.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <h4>{item.language}</h4>
              <p>
                <strong>Writing lvl: </strong>
                {item.wrlevel}
              </p>
              <p>
                <strong>Speacking lvl: </strong> {item.splevel}
              </p>
            </div>
          );
        })}
      </div>
      <h3 style={{ color: "white" }}>Skills</h3>
      <div className="skills-card">
        {habilities.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <p>{item}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
}
