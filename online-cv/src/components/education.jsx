import React from "react";
import "./education.css";

const Education = ({ education }) => {
  return (
    <div>
      <h3 style={{ color: "white" }}>Education</h3>
      <div className="education-card">
        {education.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <h4>⚖️ {item.name}</h4>
              <p>
                <strong>🏫 institution: </strong>
                {item.where}
              </p>
              <p>
                <strong>⌚ date/duration: </strong>
                {item.date}
              </p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Education;
